STEM = C64_TrueType_v1.2-STYLE
ZIP = $(STEM).zip

S = wordpress-style-dir
OUT_ZIP = twenty-fifteen-c64-child.zip

all: .get-fonts-stamp .put-fonts-stamp .dist-stamp
	@echo "The theme zip is ready. It is $(OUT_ZIP)"

.get-fonts-stamp:
	curl -O http://style64.org/file/$(ZIP)
	touch $@

.put-fonts-stamp: .get-fonts-stamp
	rm -rf $(STEM)
	unzip $(ZIP)
	cd $(STEM)/fonts; for fontfile in *.eot *.svg *.ttf *.woff *.woff2; do \
		mv $$fontfile ../../$(S)/fonts/$$(basename $$fontfile | tr A-Z a-z); \
	done
	rm -rf $(STEM)
	touch $@

.dist-stamp:
	rm -f $(OUT_ZIP)
	zip -r $(OUT_ZIP) $(S)
	touch $@

clean:
	rm -rf $(OUT_ZIP) $(STEM)
	cd $(S)/fonts; rm -f *.eot *.svg *.ttf *.woff *.woff2
	rm -f .*-stamp
